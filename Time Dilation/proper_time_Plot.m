function proper_time_Plot()

u=0:0.01:0.99;    %speed as a portion of speed of light
t0=input('Give proper time in seconds ');
%t=zeros(length(u));
for i=1:length(u)
    [t(i)]=proper_time(u(i),t0);
end

figure;
plot(u*299792458,t);
grid on;
xlabel('velocity(m/s)');
ylabel('time(s)');
title('Time dilation to velocity');

end