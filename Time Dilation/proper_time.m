function [t]=proper_time(u, t0)

if (u>=0 && u<1) && (t0>0)
    gamma=lorentz_factor(u);
    t=gamma*t0;
end

end