function lorentz_factor_Plot()

u=0:0.01:0.99;    %speed as a portion of speed of light
%g=zeros(length(u));
for i=1:length(u)
    [g(i)]=lorentz_factor(u(i));
end

figure;
plot(u*299792458,g);
grid on;
xlabel('velocity(m/s)');
ylabel('gamma');
title('Lorentz Factor');

end