Special Relativity
==================

The following .m files contain simple functions for Lorentz factor (gamma) and
the two consequences of someone moving with the speed of light: Time Dilation
and Length Contraction. There are also 2 example figures one for each occasion
(time dilation, length contraction).

These .m files were created for fun during my study for an undergraduate course
in Modern Physics using Matlab R2016b.

Note: If you find any mistake please do not hesitate to contact me.