function proper_length_Plot()

u=0:0.01:0.99;    %speed as a portion of speed of light
l0=input('Give proper length in meters ');
%l=zeros(length(u));
for i=1:length(u)
    [l(i)]=proper_length(u(i),l0);
end

figure;
plot(u*299792458,l);
grid on;
xlabel('velocity(m/s)');
ylabel('length(m)');
title('Length contraction to velocity');

end