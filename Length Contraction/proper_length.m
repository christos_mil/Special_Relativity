function [l]=proper_length(u, l0)

if (u>=0 && u<1) && (l0>0)
    gamma=lorentz_factor(u);
    l=l0/gamma;
end

end