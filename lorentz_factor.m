function [gamma]=lorentz_factor(u)

%function to calculate the Lorenz factor (gamma factor), which
%is widely used in a number of equations in special relativity

if (u<1 && u>=0)
    c=299792458;
    gamma=1/sqrt(1-((u*c)^2)/c^2);
elseif (u<0)
    disp('Velocity should be positive because we care only for its absolute value.');
else
    disp('Speed of light is the upper limit (for now)!');
end

end